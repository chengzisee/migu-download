import requests
import wx
import musicGui
import sys
from bs4 import BeautifulSoup
from fake_useragent import UserAgent

s = requests.session()

header1 = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,"
              "application/signed-exchange;v=b3;q=0.9",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "zh-CN,zh;q=0.9",
    "Connection": "keep-alive",
    "Host": "m.music.migu.cn",
    "Sec-Fetch-Dest": "document",
    "Sec-Fetch-Mode": "navigate",
    "Sec-Fetch-Site": "none",
    "Upgrade-Insecure-Requests": "1",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 "
                  "Safari/537.36 SLBrowser/7.0.0.1071 SLBChan/30 "

}

header2 = {
    "Accept": "application/json, text/javascript, */*; q=0.01",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "zh-CN,zh;q=0.9",
    "Connection": "keep-alive",
    "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
    "Host": "m.music.migu.cn",
    "Referer": "https://m.music.migu.cn/v3/search?keyword=%E6%99%B4%E5%A4%A9",
    "Sec-Fetch-Dest": "empty",
    "Sec-Fetch-Mode": "cors",
    "Sec-Fetch-Site": "same-origin",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36 SLBrowser/7.0.0.1071 SLBChan/30",
    "X-Requested-With": "XMLHttpRequest"

}

keyword = {
    "keyword": "晴天"
}

music_download = []
lyrics_download = []
show = []


class MianWindow(musicGui.MyFrame1):
    def btn_click(self, event):
        if len(self.m_textCtrl1.Value) <= 0:
            wx.MessageBox('输入不得为空', '警告', wx.OK | wx.ICON_ERROR)
        else:
            keyword["keyword"] = self.m_textCtrl1.Value
            getMusic()
            self.m_listBox2.Clear()
            self.m_listBox2.AppendItems(show)

    def item_selected(self, event):
        download_music(self.m_listBox2.GetSelection())


def download_music(index):
    mymusic = s.get(music_download[index])
    if open(show[index] + ".mp3", 'wb').write(mymusic.content):
        wx.MessageBox('下载成功', '提示')
    else:
        wx.MessageBox('下载失败', '提示', wx.OK | wx.ICON_ERROR)

def getMusicList(key):
    musiclist = s.get("https://m.music.migu.cn/migu/remoting/scr_search_tag?rows=20&type=2&keyword=" + key + "&pgc=1",
                      headers=header2)
    musiclist_json = musiclist.json()
    for single in musiclist_json["musics"]:
        print(single["songName"], single["singerName"], single["mp3"])
        music_download.append(single["mp3"])
        lyrics_download.append(single["lyrics"])
        show.append(single["songName"] + "----" + single["singerName"])


def getMusic():
    music_download.clear()
    lyrics_download.clear()
    show.clear()
    s.get("https://m.music.migu.cn/migu/remoting/is_login_tag?u=1", headers=header1)
    t = s.get("https://m.music.migu.cn/v3/search", headers=header1, params=keyword)
    print(keyword["keyword"])
    getMusicList(keyword["keyword"])


if __name__ == '__main__':
    # keyword["keyword"] = "烟花易冷"
    # getMusic()
    app = wx.App()
    ua = UserAgent()
    newUa = ua.random
    header1["User-Agent"] = newUa
    header2["User-Agent"] = newUa
    main_win = MianWindow(None)
    main_win.Show()
    app.MainLoop()
